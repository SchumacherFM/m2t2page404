## Magento 2 Trained Partner Program: Developer Exercises 

# Unit Two. Controllers

Show different 404 pages for the following (Training2_Specific404Page):

### A not-found product (catalog/product/view/id/_ID_) 

[http://mage2.local/catalog/product/view/id/2342343](http://mage2.local/catalog/product/view/id/2342343)

You should see: `Product not found via SchumacherFM` in `h1`.

### A category (catalog/category/view/id/_ID_) 

[http://mage2.local/gear/bags.html](http://mage2.local/gear/bags.html)

You should see: `Found Category via SchumacherFM` in `h1`.

This is not the solution 8-|.

### Another page

Change the config in General -> Web -> Default pages
