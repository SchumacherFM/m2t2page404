<?php

namespace SchumacherFM\M2T2Page404\Block;

class Product extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * Prepare HTML content
     *
     * @return string
     */
    protected function _toHtml()
    {
        return '<h1>Product not found via SchumacherFM</h1>';
    }

}
