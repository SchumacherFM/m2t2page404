<?php

namespace SchumacherFM\M2T2Page404\Block;

class CategoryView extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * Prepare HTML content
     *
     * @return string
     */
    protected function _toHtml()
    {
        return '<h1>Found Category via SchumacherFM</h1>';
    }

}
